﻿namespace Paises_RC30.Serviços
{

    using Newtonsoft.Json;
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class APIService
    {
        List<Paises> Paises = new List<Paises>();
        /// <summary>
        /// Vai buscar todos os dados da API para uma Lista.
        /// </summary>
        /// <param name="urlbase"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        public async Task<Response> GetPaises(string urlbase, string controller)
        {

            try
            {

                var Cliente = new HttpClient();
                Cliente.BaseAddress = new Uri(urlbase);

                var resposta = await Cliente.GetAsync(controller);
                var resultado = await resposta.Content.ReadAsStringAsync();

                if (!resposta.IsSuccessStatusCode)
                {

                    return new Response { IsSuccess = false, Message = resultado };

                }

                Paises = JsonConvert.DeserializeObject<List<Paises>>(resultado);
                return new Response { IsSuccess = true, result = Paises };
        
            }
            catch (Exception e)
            {

                return  new Response{ IsSuccess = false, Message = e.Message };

            }
        }

    }
}
