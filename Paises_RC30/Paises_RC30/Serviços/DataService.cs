﻿namespace Paises_RC30.Serviços
{

    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;

    public class DataService
    {

        #region atributos

        /// <summary>
        /// Para ter uma connexão à BD.
        /// </summary>
        private SQLiteConnection Connection;

        /// <summary>
        /// Para inserir na base de dados.
        /// </summary>
        private SQLiteCommand Command;

        /// <summary>
        /// Para mostrar mensagens.
        /// </summary>
        private DialogService DialogService = new DialogService();

        #endregion

        /// <summary>
        /// Construtor que vai criar uma pasta 'Data'.
        /// </summary>
        public DataService()
        {

            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }

            //Onde está localizada a base de dados:
            var Path = @"Data\PaisesBD.sqlite";

            try
            {

                Connection = new SQLiteConnection("Data Source=" + Path);
                Connection.Open();

                //Tabela principal:
                string Sqlcommand = "CREATE TABLE IF NOT EXISTS Countries(name varchar(250), capital varchar(250), " +
                    "region varchar(250), subregion varchar(250), population INT, nativeName varchar(250), " +
                    "alpha2Code varchar(250), demonym varchar(250), area varchar(250), gini varchar(250), " +
                    "numericCode varchar(250), Langname varchar(500), iso639_1 varchar(250), code varchar(500), Currname varchar(500), symbol varchar(500)," +
                    "acronym varchar(500), blocname varchar(500), topLevelDomain varchar(500), Latitude varchar(500), Longitude varchar(500)," +
                    "callcode varchar(500), border varchar(500), timezone varchar(500))";

                Command = new SQLiteCommand(Sqlcommand, Connection);
                Command.ExecuteNonQuery();

            }
            catch (Exception e)
            {

                DialogService.ShowMessage(e.Message + "\n" + e.Source, "Erro", MessageBoxButtons.OK);

            }

        }

        /// <summary>
        /// Gravar os dados na base de dados.
        /// </summary>
        /// <param name="Paises"></param>
        public void SaveData(List<Paises> Paises)
        {

            List<Paises> Lista = Paises;
            List<Currency> Moedas = new List<Currency>();
            List<string> leveldomain = new List<string>();
            List<string> timezones = new List<string>();
            List<string> telephones = new List<string>();
            List<string> LatLng = new List<string>();
            List<string> borders = new List<string>();
            List<Language> Lingua = new List<Language>();
            List<RegionalBloc> Regional = new List<RegionalBloc>();

            try
            {

                foreach (var a in Lista)
                {

                    #region Tabela principal

                    if (a.name.Contains('\''))
                    {
                        a.name.Replace("'", "\'");
                    }

                    #region Concatenação:

                    string lang_name = "";
                    string lang_iso = "";
                    string curr_code = "";
                    string curr_name = "";
                    string curr_sym = "";
                    string bloc_name = "";
                    string bloc_acronym = "";
                    string domain = "";
                    string lat = "";
                    string lon = "";
                    string call = "";
                    string border = "";
                    string time = "";

                    Lingua = a.languages;
                    Moedas = a.currencies;
                    Regional = a.regionalBlocs;
                    leveldomain = a.topLevelDomain;
                    LatLng = a.latlng;
                    telephones = a.callingCodes;
                    borders = a.borders;
                    timezones = a.timezones;

                    #region Languages:

                    foreach (var L in Lingua)
                    {

                        lang_name += L.name;
                        lang_iso += L.iso639_1;

                        if (Lingua.IndexOf(L) != Lingua.Count - 1)
                        {

                            lang_name += " e ";
                            lang_iso += " e ";

                        }

                    }

                    #endregion

                    #region Currencies:

                    foreach (var C in Moedas)
                    {

                        curr_code += C.code;
                        curr_name += C.name;
                        curr_sym += C.symbol;

                        if (Moedas.IndexOf(C) != Moedas.Count - 1)
                        {

                            curr_code += " e ";
                            curr_name += " e ";
                            curr_sym += " e ";

                        }

                    }

                    #endregion

                    #region RegionalBlocs:

                    foreach (var R in Regional)
                    {

                        bloc_acronym += R.acronym;
                        bloc_name += R.name;

                        if (Regional.IndexOf(R) != Regional.Count - 1)
                        {

                            bloc_acronym += " e ";
                            bloc_name += " e ";


                        }

                    }

                    #endregion

                    #region Domain:

                    foreach (var D in leveldomain)
                    {

                        domain += D;

                        if (leveldomain.IndexOf(D) != leveldomain.Count - 1)
                        {

                            domain += " e ";

                        }

                    }


                    #endregion

                    #region Latitude e Longitude:

                    if (a.latlng.Count != 0)
                    {

                        lat += LatLng[0];
                        lon += LatLng[1];

                    }

                    #endregion

                    #region call:

                    foreach (var CA in telephones)
                    {

                        call += CA;

                        if (telephones.IndexOf(CA) != telephones.Count - 1)
                        {

                            call += " e ";

                        }

                    }

                    #endregion

                    #region Borders:

                    foreach (var B in borders)
                    {

                        border += B;

                        if (borders.IndexOf(B) != borders.Count - 1)
                        {

                            border += " e ";

                        }

                    }

                    #endregion

                    #region time:

                    foreach (var T in timezones)
                    {

                        time += T;

                        if (timezones.IndexOf(T) != timezones.Count - 1)
                        {

                            time += " e ";

                        }

                    }

                    #endregion

                    #endregion

                    string Sql = "INSERT INTO Countries(name, capital, region, subregion, population, nativeName, alpha2Code, demonym, area, gini, numericCode,Langname, "
                                        + "iso639_1, code, Currname, symbol, acronym, blocname, topLevelDomain, Latitude, Longitude,"
                                        + "callcode, border, timezone) "
                                        + "values(@name, @capital, @region, @subregion, @population, @nativeName, @alpha2code, @demonym, @area, @gini, @numericCode, "
                                        + "@Langname, @iso639_1, @code, @Currname, @symbol, @acronym, @blocname, @topLevelDomain, @Latitude, @Longitude,"
                                        + "@callcode, @border, @timezone)";

                    Command = new SQLiteCommand(Sql, Connection);

                    Command.Parameters.AddWithValue("@name", a.name);


                    if (string.IsNullOrWhiteSpace(a.capital))
                    {

                        Command.Parameters.AddWithValue("@capital", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@capital", a.capital);

                    }

                    if (string.IsNullOrWhiteSpace(a.region))
                    {

                        Command.Parameters.AddWithValue("@region", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@region", a.region);

                    }

                    if (string.IsNullOrWhiteSpace(a.subregion))
                    {

                        Command.Parameters.AddWithValue("@subregion", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@subregion", a.subregion);

                    }

                    if (string.IsNullOrWhiteSpace(a.population.ToString()))
                    {

                        Command.Parameters.AddWithValue("@population", 0);

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@population", a.population);

                    }

                    if (string.IsNullOrWhiteSpace(a.nativeName))
                    {

                        Command.Parameters.AddWithValue("@nativeName", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@nativeName", a.nativeName);

                    }

                    if (string.IsNullOrWhiteSpace(a.alpha2Code))
                    {

                        Command.Parameters.AddWithValue("@alpha2Code", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@alpha2Code", a.alpha2Code);

                    }

                    if (string.IsNullOrWhiteSpace(a.demonym))
                    {

                        Command.Parameters.AddWithValue("@demonym", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@demonym", a.demonym);

                    }

                    if (string.IsNullOrWhiteSpace(a.area))
                    {

                        Command.Parameters.AddWithValue("@area", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@area", a.area);

                    }

                    if (string.IsNullOrWhiteSpace(a.gini))
                    {

                        Command.Parameters.AddWithValue("@gini", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@gini", a.gini);

                    }

                    if (string.IsNullOrWhiteSpace(a.numericCode))
                    {

                        Command.Parameters.AddWithValue("@numericCode", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@numericCode", a.numericCode);

                    }

                    if (string.IsNullOrWhiteSpace(lang_name))
                    {

                        Command.Parameters.AddWithValue("@Langname", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@Langname", lang_name);

                    }

                    if (string.IsNullOrWhiteSpace(lang_iso))
                    {

                        Command.Parameters.AddWithValue("@iso639_1", "N/A");

                    }
                    else
                    {
                        Command.Parameters.AddWithValue("@iso639_1", lang_iso);
                    }

                    if (string.IsNullOrWhiteSpace(curr_code) || string.IsNullOrWhiteSpace(curr_name) || string.IsNullOrWhiteSpace(curr_sym))
                    {

                        Command.Parameters.AddWithValue("@code", "N/A");
                        Command.Parameters.AddWithValue("@Currname", "N/A");
                        Command.Parameters.AddWithValue("@symbol", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@code", curr_code);
                        Command.Parameters.AddWithValue("@Currname", curr_name);
                        Command.Parameters.AddWithValue("@symbol", curr_sym);

                    }

                    if (string.IsNullOrWhiteSpace(bloc_acronym))
                    {

                        Command.Parameters.AddWithValue("@acronym", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@acronym", bloc_acronym);

                    }

                    if (string.IsNullOrWhiteSpace(bloc_name))
                    {

                        Command.Parameters.AddWithValue("@blocname", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@blocname", bloc_name);

                    }

                    if (string.IsNullOrWhiteSpace(domain))
                    {

                        Command.Parameters.AddWithValue("@topLevelDomain", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@topLevelDomain", domain);

                    }

                    if (string.IsNullOrWhiteSpace(lat) || string.IsNullOrWhiteSpace(lon))
                    {

                        Command.Parameters.AddWithValue("@Latitude", "N/A");
                        Command.Parameters.AddWithValue("@Longitude", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@Latitude", lat);
                        Command.Parameters.AddWithValue("@Longitude", lon);

                    }

                    if (string.IsNullOrWhiteSpace(call))
                    {

                        Command.Parameters.AddWithValue("@callcode", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@callcode", call);

                    }

                    if (string.IsNullOrWhiteSpace(border))
                    {

                        Command.Parameters.AddWithValue("@border", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@border", border);

                    }

                    if (string.IsNullOrWhiteSpace(time))
                    {

                        Command.Parameters.AddWithValue("@timezone", "N/A");

                    }
                    else
                    {

                        Command.Parameters.AddWithValue("@timezone", time);

                    }

                    Command.ExecuteNonQuery();

                    Moedas = null;
                    leveldomain = null;
                    timezones = null;
                    telephones = null;
                    LatLng = null;
                    borders = null;
                    Lingua = null;
                    Regional = null;


                    #endregion                   

                }

            }
            catch (Exception e)
            {

                DialogService.ShowMessage(e.Message, "Erro", MessageBoxButtons.RetryCancel);

            }

        }

        /// <summary>
        /// Vai buscar os dados da base de dados.
        /// </summary>
        /// <returns></returns>
        public List<Paises> GetData()
        {

            List<Paises> Paises = new List<Paises>();

            try
            {
                string Sql = "SELECT name, capital, region, subregion, population, nativeName, alpha2Code, demonym, area, gini, numericCode, Langname, "
                                        + "iso639_1, code, Currname, symbol, acronym, blocname, topLevelDomain, Latitude, Longitude,"
                                        + "callcode, border, timezone FROM Countries";

                Command = new SQLiteCommand(Sql, Connection);
                SQLiteDataReader reader = Command.ExecuteReader();

                while (reader.Read())
                {

                    List<string> a = new List<string>();
                    List<string> b = new List<string>();
                    List<string> c = new List<string>();
                    List<string> d = new List<string>();

                    a.Add((string)reader["border"]);
                    b.Add((string)reader["timezone"]);
                    c.Add((string)reader["topLevelDomain"]);
                    d.Add((string)reader["callcode"]);

                Paises.Add(new Paises
                {

                    name = (string)reader["name"],
                    capital = (string)reader["capital"],
                    region = (string)reader["region"],
                    subregion = (string)reader["subregion"],
                    population = (int)reader["population"],
                    nativeName = (string)reader["nativeName"],
                    alpha2Code = (string)reader["alpha2Code"],
                    demonym = (string)reader["demonym"],
                    area = (string)reader["area"],
                    gini = (string)reader["gini"],
                    numericCode = (string)reader["numericCode"],
                    Lang = (string)reader["Langname"] + "(" + reader["iso639_1"] + ")",
                    Curr = (string)reader["Currname"] + " (" + reader["code"] + " - " + reader["symbol"] + ")",
                    Regio_acron = (string)reader["acronym"],
                    Regio_name = (string)reader["blocname"],
                    topLevelDomain = c,
                    Lati = (string)reader["Latitude"],
                    Long = (string)reader["Longitude"],
                    callingCodes = d,
                    borders = a,
                    timezones = b,                 

                    });

                }      

            Connection.Close();

                return Paises;

        }
            catch (Exception e)
            {

                DialogService.ShowMessage(e.Message, "Erro", MessageBoxButtons.OK);
                return null;

            }

}

        /// <summary>
        /// Apaga os dados para não ocorrer duplicação de dados.
        /// </summary>
        public void DeleteData()
        {

            try
            {

                string Sql = "DELETE FROM Countries";

                Command = new SQLiteCommand(Sql, Connection);
                Command.ExecuteNonQuery();


            }
            catch (Exception e)
            {

                DialogService.ShowMessage(e.Message + "\n" + e.Source, "Erro", MessageBoxButtons.OK);

            }

        }


    }
}
