﻿namespace Paises_RC30.Serviços
{

    using Modelos;
    using System.Net;

    public class NetworkService
    {

        DialogService DialogService = new DialogService();

        /// <summary>
        /// Ver se a connexão à internet é possivél.
        /// </summary>
        /// <returns></returns>
        public Response Connection()
        {

            var cliente = new WebClient();

            try
            {
                using (cliente.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return new Response { IsSuccess = true };
                }
            }
            catch
            {
                return new Response { IsSuccess = false, Message = "Não se consegue estabelecer uma ligação à internet." };
            }

        }


    }
}
