﻿namespace Paises_RC30.Serviços
{

    using System.Windows.Forms;

    public class DialogService
    {

        /// <summary>
        /// Amostra uma mensagem personalizada (Mensagem, Título e os butões).
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="titulo"></param>
        /// <param name="button"></param>
        public void ShowMessage (string msg, string titulo, MessageBoxButtons button)
        {
            MessageBox.Show(msg, titulo, button);
        }
        
    }
}
