﻿namespace Paises_RC30
{
    partial class Form_Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Principal));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Label_denomyn = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Label_area = new System.Windows.Forms.Label();
            this.Label_population = new System.Windows.Forms.Label();
            this.Label_timezones = new System.Windows.Forms.Label();
            this.Label_borders = new System.Windows.Forms.Label();
            this.Label_acronym = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.comboBox_countryselect = new System.Windows.Forms.ComboBox();
            this.comboBox_regionselect = new System.Windows.Forms.ComboBox();
            this.Label_lat = new System.Windows.Forms.Label();
            this.Label_lng = new System.Windows.Forms.Label();
            this.Label_capital = new System.Windows.Forms.Label();
            this.Label_Region = new System.Windows.Forms.Label();
            this.Label_Subregion = new System.Windows.Forms.Label();
            this.Label_regionalBloc = new System.Windows.Forms.Label();
            this.Label_numericcode = new System.Windows.Forms.Label();
            this.Label_name = new System.Windows.Forms.Label();
            this.Label_nativename = new System.Windows.Forms.Label();
            this.Label_Currencies = new System.Windows.Forms.Label();
            this.Label_language = new System.Windows.Forms.Label();
            this.Label_Telephones = new System.Windows.Forms.Label();
            this.Label_topleveldomains = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PictureBox_Flag = new System.Windows.Forms.PictureBox();
            this.Button_Sair = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.acercaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Label_gini = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Flag)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1});
            this.statusStrip1.Name = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            resources.ApplyResources(this.toolStripStatusLabel1, "toolStripStatusLabel1");
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            resources.ApplyResources(this.toolStripProgressBar1, "toolStripProgressBar1");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Label_gini);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.Label_denomyn);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.Label_area);
            this.panel1.Controls.Add(this.Label_population);
            this.panel1.Controls.Add(this.Label_timezones);
            this.panel1.Controls.Add(this.Label_borders);
            this.panel1.Controls.Add(this.Label_acronym);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.comboBox_countryselect);
            this.panel1.Controls.Add(this.comboBox_regionselect);
            this.panel1.Controls.Add(this.Label_lat);
            this.panel1.Controls.Add(this.Label_lng);
            this.panel1.Controls.Add(this.Label_capital);
            this.panel1.Controls.Add(this.Label_Region);
            this.panel1.Controls.Add(this.Label_Subregion);
            this.panel1.Controls.Add(this.Label_regionalBloc);
            this.panel1.Controls.Add(this.Label_numericcode);
            this.panel1.Controls.Add(this.Label_name);
            this.panel1.Controls.Add(this.Label_nativename);
            this.panel1.Controls.Add(this.Label_Currencies);
            this.panel1.Controls.Add(this.Label_language);
            this.panel1.Controls.Add(this.Label_Telephones);
            this.panel1.Controls.Add(this.Label_topleveldomains);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.PictureBox_Flag);
            this.panel1.Controls.Add(this.Button_Sair);
            this.panel1.Controls.Add(this.menuStrip1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // Label_denomyn
            // 
            resources.ApplyResources(this.Label_denomyn, "Label_denomyn");
            this.Label_denomyn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_denomyn.Name = "Label_denomyn";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // Label_area
            // 
            resources.ApplyResources(this.Label_area, "Label_area");
            this.Label_area.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_area.Name = "Label_area";
            // 
            // Label_population
            // 
            resources.ApplyResources(this.Label_population, "Label_population");
            this.Label_population.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_population.Name = "Label_population";
            // 
            // Label_timezones
            // 
            this.Label_timezones.AutoEllipsis = true;
            resources.ApplyResources(this.Label_timezones, "Label_timezones");
            this.Label_timezones.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_timezones.Name = "Label_timezones";
            // 
            // Label_borders
            // 
            this.Label_borders.AutoEllipsis = true;
            resources.ApplyResources(this.Label_borders, "Label_borders");
            this.Label_borders.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_borders.Name = "Label_borders";
            // 
            // Label_acronym
            // 
            this.Label_acronym.AutoEllipsis = true;
            resources.ApplyResources(this.Label_acronym, "Label_acronym");
            this.Label_acronym.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_acronym.Name = "Label_acronym";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // comboBox_countryselect
            // 
            this.comboBox_countryselect.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBox_countryselect.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox_countryselect.FormattingEnabled = true;
            resources.ApplyResources(this.comboBox_countryselect, "comboBox_countryselect");
            this.comboBox_countryselect.Name = "comboBox_countryselect";
            this.comboBox_countryselect.SelectedIndexChanged += new System.EventHandler(this.comboBox_countryselect_SelectedIndexChanged);
            // 
            // comboBox_regionselect
            // 
            this.comboBox_regionselect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_regionselect.FormattingEnabled = true;
            resources.ApplyResources(this.comboBox_regionselect, "comboBox_regionselect");
            this.comboBox_regionselect.Name = "comboBox_regionselect";
            this.comboBox_regionselect.SelectedIndexChanged += new System.EventHandler(this.comboBox_regionselect_SelectedIndexChanged);
            // 
            // Label_lat
            // 
            resources.ApplyResources(this.Label_lat, "Label_lat");
            this.Label_lat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_lat.Name = "Label_lat";
            // 
            // Label_lng
            // 
            resources.ApplyResources(this.Label_lng, "Label_lng");
            this.Label_lng.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_lng.Name = "Label_lng";
            // 
            // Label_capital
            // 
            resources.ApplyResources(this.Label_capital, "Label_capital");
            this.Label_capital.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_capital.Name = "Label_capital";
            // 
            // Label_Region
            // 
            resources.ApplyResources(this.Label_Region, "Label_Region");
            this.Label_Region.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_Region.Name = "Label_Region";
            // 
            // Label_Subregion
            // 
            resources.ApplyResources(this.Label_Subregion, "Label_Subregion");
            this.Label_Subregion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_Subregion.Name = "Label_Subregion";
            // 
            // Label_regionalBloc
            // 
            this.Label_regionalBloc.AutoEllipsis = true;
            resources.ApplyResources(this.Label_regionalBloc, "Label_regionalBloc");
            this.Label_regionalBloc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_regionalBloc.Name = "Label_regionalBloc";
            // 
            // Label_numericcode
            // 
            resources.ApplyResources(this.Label_numericcode, "Label_numericcode");
            this.Label_numericcode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_numericcode.Name = "Label_numericcode";
            // 
            // Label_name
            // 
            resources.ApplyResources(this.Label_name, "Label_name");
            this.Label_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_name.Name = "Label_name";
            // 
            // Label_nativename
            // 
            resources.ApplyResources(this.Label_nativename, "Label_nativename");
            this.Label_nativename.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_nativename.Name = "Label_nativename";
            // 
            // Label_Currencies
            // 
            this.Label_Currencies.AutoEllipsis = true;
            resources.ApplyResources(this.Label_Currencies, "Label_Currencies");
            this.Label_Currencies.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_Currencies.Name = "Label_Currencies";
            // 
            // Label_language
            // 
            this.Label_language.AutoEllipsis = true;
            resources.ApplyResources(this.Label_language, "Label_language");
            this.Label_language.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_language.Name = "Label_language";
            // 
            // Label_Telephones
            // 
            resources.ApplyResources(this.Label_Telephones, "Label_Telephones");
            this.Label_Telephones.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_Telephones.Name = "Label_Telephones";
            // 
            // Label_topleveldomains
            // 
            resources.ApplyResources(this.Label_topleveldomains, "Label_topleveldomains");
            this.Label_topleveldomains.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_topleveldomains.Name = "Label_topleveldomains";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label3.Name = "label3";
            // 
            // PictureBox_Flag
            // 
            resources.ApplyResources(this.PictureBox_Flag, "PictureBox_Flag");
            this.PictureBox_Flag.Name = "PictureBox_Flag";
            this.PictureBox_Flag.TabStop = false;
            // 
            // Button_Sair
            // 
            this.Button_Sair.BackColor = System.Drawing.Color.Transparent;
            this.Button_Sair.Image = global::Paises_RC30.Properties.Resources.Cancelar;
            resources.ApplyResources(this.Button_Sair, "Button_Sair");
            this.Button_Sair.Name = "Button_Sair";
            this.Button_Sair.UseVisualStyleBackColor = false;
            this.Button_Sair.Click += new System.EventHandler(this.Button_Sair_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.acercaToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // acercaToolStripMenuItem
            // 
            resources.ApplyResources(this.acercaToolStripMenuItem, "acercaToolStripMenuItem");
            this.acercaToolStripMenuItem.Name = "acercaToolStripMenuItem";
            this.acercaToolStripMenuItem.Click += new System.EventHandler(this.acercaToolStripMenuItem_Click);
            // 
            // Label_gini
            // 
            resources.ApplyResources(this.Label_gini, "Label_gini");
            this.Label_gini.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_gini.Name = "Label_gini";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // Form_Principal
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "Form_Principal";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Flag)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox comboBox_countryselect;
        private System.Windows.Forms.ComboBox comboBox_regionselect;
        private System.Windows.Forms.Label Label_lat;
        private System.Windows.Forms.Label Label_lng;
        private System.Windows.Forms.Label Label_capital;
        private System.Windows.Forms.Label Label_Region;
        private System.Windows.Forms.Label Label_Subregion;
        private System.Windows.Forms.Label Label_regionalBloc;
        private System.Windows.Forms.Label Label_numericcode;
        private System.Windows.Forms.Label Label_name;
        private System.Windows.Forms.Label Label_nativename;
        private System.Windows.Forms.Label Label_Currencies;
        private System.Windows.Forms.Label Label_language;
        private System.Windows.Forms.Label Label_Telephones;
        private System.Windows.Forms.Label Label_topleveldomains;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox PictureBox_Flag;
        private System.Windows.Forms.Button Button_Sair;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem acercaToolStripMenuItem;
        private System.Windows.Forms.Label Label_area;
        private System.Windows.Forms.Label Label_population;
        private System.Windows.Forms.Label Label_timezones;
        private System.Windows.Forms.Label Label_borders;
        private System.Windows.Forms.Label Label_acronym;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label Label_denomyn;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label Label_gini;
        private System.Windows.Forms.Label label20;
    }
}

