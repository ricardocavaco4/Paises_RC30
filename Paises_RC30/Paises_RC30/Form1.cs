﻿namespace Paises_RC30
{
    using Modelos;
    using Serviços;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Linq;
    using Svg;
    using System.Net;
    using System.Drawing.Imaging;
    using System.Drawing;
    using ImageMagick;

    public partial class Form_Principal : Form
    {

        #region instâncias e atributos

        /// <summary>
        /// Vendo por que lado se carregou o programa.
        /// </summary>
        bool Load;

        /// <summary>
        /// Caminho para as imagens.
        /// </summary>
        string path;

        /// <summary>
        /// Para ter os valores dos paises.
        /// </summary>
        private List<Paises> ListaPaises { get; set; }

        /// <summary>
        /// Para mostrar mensagens.
        /// </summary>
        DialogService DialogService;

        /// <summary>
        /// Para testar a conexão à internet.
        /// </summary>
        NetworkService NetworkService;

        /// <summary>
        /// Para obter os dados da API.
        /// </summary>
        APIService APIService;

        /// <summary>
        /// Para ter acesso à base de dados.
        /// </summary>
        DataService DataService;

        #endregion

        #region Construtor

        public Form_Principal()
        {

            InitializeComponent();
            NetworkService = new NetworkService();
            APIService = new APIService();
            DataService = new DataService();
            DialogService = new DialogService();
            ListaPaises = new List<Paises>();
            LoadData();

        }

        #endregion

        #region Eventos do Form

        /// <summary>
        /// Sai da aplicação.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Sair_Click(object sender, EventArgs e)
        {

            Application.Exit();
            
        }

        /// <summary>
        /// Mostra a última versão e data de edição, tal como o autor do trabalho.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void acercaToolStripMenuItem_Click(object sender, EventArgs e)
        {

            DialogService.ShowMessage("Última edição: 07/12/2017\nFeito por: Ricardo Cavaco, CET30", "Versão 1.0.1", MessageBoxButtons.OK);

        }

        /// <summary>
        /// Carrega os paises conforme o continente selecionado.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_regionselect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(comboBox_regionselect.Text))
            {

                Label_name.Text = "- -";
                Label_nativename.Text = "- -";
                Label_numericcode.Text = "- -";
                Label_area.Text = "- -";
                Label_population.Text = "- -";
                Label_capital.Text = "- -";
                Label_Region.Text = "- -";
                Label_Subregion.Text = "- -";
                Label_denomyn.Text = "- -";
                Label_gini.Text = "- -";
                Label_lat.Text = " - -";
                Label_lng.Text = " - -";
                Label_Currencies.Text = "- -";
                Label_language.Text = "- -";
                Label_acronym.Text = "- -";
                Label_regionalBloc.Text = "- -";
                Label_Telephones.Text = "- -";
                Label_borders.Text = "- -";
                Label_timezones.Text = "- -";
                Label_topleveldomains.Text = "- -";
                PictureBox_Flag.Image = null;
                comboBox_countryselect.DataSource = null;
                comboBox_countryselect.Enabled = false;

            }
            else
            {

                comboBox_countryselect.Enabled = true;

                if (comboBox_regionselect.Text == "All")
                {

                    var cont = from p in ListaPaises select p;
                    comboBox_countryselect.DataSource = cont.ToList();
                    comboBox_countryselect.DisplayMember = "Name";


                }
                else
                {

                    var cont = from p in ListaPaises where p.region == comboBox_regionselect.Text select p;
                    comboBox_countryselect.DataSource = cont.ToList();
                    comboBox_countryselect.DisplayMember = "Name";

                }
            }


        }

        /// <summary>
        /// Mostra os dados do país selecionado no Form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_countryselect_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (!string.IsNullOrWhiteSpace(comboBox_regionselect.Text))
            {

                if (comboBox_regionselect.Items.Count != 0)
                {

                    #region atribuir valores

                    Paises p = new Paises();

                    if (comboBox_regionselect.Text == "All")
                    {

                        p = ListaPaises[comboBox_countryselect.SelectedIndex];

                    }
                    else
                    {

                        var x = (from pais in ListaPaises
                                 where pais.region == comboBox_regionselect.Text
                                 select pais).ToList();
                        p = x[comboBox_countryselect.SelectedIndex];


                    }

                    Label_name.Text = p.name + " (" + p.alpha2Code + ")";
                    Label_nativename.Text = p.nativeName;
                    Label_numericcode.Text = p.numericCode;
                    Label_area.Text = p.area;
                    Label_population.Text = p.population.ToString();
                    Label_capital.Text = p.capital;
                    Label_Region.Text = p.region;
                    Label_Subregion.Text = p.subregion;
                    Label_denomyn.Text = p.demonym;
                    Label_gini.Text = p.gini;

                    long Tamanho = new FileInfo(@"Data\PaisesBD.sqlite").Length;
                    if (Load == false)
                    {

                        Label_lat.Text = p.Lati;
                        Label_lng.Text = p.Long;
                        if (p.borders.Count != 0)
                        {
                            Label_borders.Text = p.borders[0];
                        }
                        if (p.callingCodes.Count != 0)
                        {
                            Label_Telephones.Text = p.callingCodes[0];
                        }
                        if (p.timezones.Count != 0)
                        {
                            Label_timezones.Text = p.timezones[0];
                        }
                        if (p.topLevelDomain.Count != 0)
                        {
                            Label_topleveldomains.Text = p.topLevelDomain[0];
                        }

                        Label_language.Text = p.Lang;
                        Label_Currencies.Text = p.Curr;
                        Label_acronym.Text = p.Regio_acron;
                        Label_regionalBloc.Text = p.Regio_name;

                    }
                    else
                    {
                        List<string> LTLG = new List<string>();
                        LTLG = p.latlng;

                        if (LTLG.Count == 0)
                        {

                            Label_lat.Text = "N/A";
                            Label_lng.Text = "N/A";

                        }
                        else
                        {

                            Label_lat.Text = LTLG[0];
                            Label_lng.Text = LTLG[1];

                        }

                        #region Prefixos, fronteiras, timezones e topleveldomains

                        List<string> PR = new List<string>();
                        List<string> BRD = new List<string>();
                        List<string> TZ = new List<string>();
                        List<string> TLD = new List<string>();

                        PR = p.callingCodes;
                        BRD = p.borders;
                        TZ = p.timezones;
                        TLD = p.topLevelDomain;

                        Label_Telephones.Text = null;
                        Label_borders.Text = null;
                        Label_timezones.Text = null;
                        Label_topleveldomains.Text = null;

                        foreach (var a in PR)
                        {

                            Label_Telephones.Text += a;

                            if (PR.IndexOf(a) != PR.Count - 1)
                            {

                                Label_Telephones.Text += " e ";

                            }

                        }

                        foreach (var a in BRD)
                        {

                            Label_borders.Text += a;

                            if (BRD.IndexOf(a) != BRD.Count - 1)
                            {

                                Label_borders.Text += " e ";

                            }

                        }

                        foreach (var a in TZ)
                        {

                            Label_timezones.Text += a;

                            if (TZ.IndexOf(a) != TZ.Count - 1)
                            {

                                Label_timezones.Text += " e ";

                            }

                            if (TZ.IndexOf(a) == 4 || TZ.IndexOf(a) == 6)
                            {
                                Label_timezones.Text += "\n";
                            }

                        }

                        foreach (var a in TLD)
                        {

                            Label_topleveldomains.Text += a;

                            if (TLD.IndexOf(a) != TLD.Count - 1)
                            {

                                Label_topleveldomains.Text += " e ";

                            }

                        }


                        #endregion

                        #region RegionalBlock

                        List<RegionalBloc> RB = new List<RegionalBloc>();
                        RB = p.regionalBlocs;

                        Label_acronym.Text = null;
                        Label_regionalBloc.Text = null;

                        foreach (var a in RB)
                        {

                            Label_acronym.Text += a.acronym;
                            Label_regionalBloc.Text += a.name;

                            if (RB.IndexOf(a) != RB.Count - 1)
                            {

                                Label_acronym.Text += " e ";
                                Label_regionalBloc.Text += " e ";

                            }

                        }

                        #endregion

                        #region Languages

                        List<Language> LG = new List<Language>();
                        LG = p.languages;

                        Label_language.Text = null;

                        foreach (var a in LG)
                        {

                            Label_language.Text += a.name + " (" + a.iso639_1 + ")";

                            if (LG.IndexOf(a) != LG.Count - 1)
                            {

                                Label_language.Text += " e ";

                            }

                        }

                        #endregion

                        #region Currencies

                        List<Currency> CUR = new List<Currency>();

                        CUR = p.currencies;

                        Label_Currencies.Text = null;

                        foreach (var a in CUR)
                        {

                            Label_Currencies.Text += string.Format("{0} ({1} - {2})", a.name, a.code, a.symbol);

                            if (CUR.IndexOf(a) != CUR.Count - 1)
                            {

                                Label_Currencies.Text += " e ";

                            }

                            if (CUR.IndexOf(a) == 3 || CUR.IndexOf(a) == 5 || CUR.IndexOf(a) == 7 || CUR.IndexOf(a) == 9)
                            {
                                Label_Currencies.Text += "\n";
                            }

                        }

                        #endregion

                    }

                    PictureBox_Flag.ImageLocation = Application.StartupPath + @"\Imagens\" + p.name + ".png";                    

                    #endregion

                }

            }

        }

        #endregion

        #region Metodos

        /// <summary>
        /// Carrega os dados para o Form, caso seja possivél.
        /// </summary>
        private async void LoadData()
        {

            var connect = NetworkService.Connection();

            long Tamanho = new FileInfo(@"Data\PaisesBD.sqlite").Length;
            if (Tamanho != 8192)
            {

                LoadOffline();
                Load = false;
            }
            else
            {
                if (connect.IsSuccess)
                {

                    await LoadOnline();
                    Load = true;
                    toolStripStatusLabel1.Text = "Carregada da API a " + DateTime.Now;

                }
                else
                {

                    DialogService.ShowMessage("Não foi possivél connectar à base de dados ou à internet.", "Ligação impossivél", MessageBoxButtons.OK);
                    return;

                }


            }

            toolStripProgressBar1.Visible = false;

        }

        /// <summary>
        /// Carrega os paises da API.
        /// </summary>
        private async Task LoadOnline()
        {

            panel1.Enabled = false;
            toolStripProgressBar1.Value = 20;

            Response r = await APIService.GetPaises("https://restcountries.eu", "rest/v2/all");
            ListaPaises = (List<Paises>)r.result;

            toolStripProgressBar1.Value = 65;

            toolStripProgressBar1.Value = 85;



            if (!Directory.Exists("Imagens"))
            {
                Directory.CreateDirectory("Imagens");
            }
            
            foreach (var p in ListaPaises)
            {

                path = Application.StartupPath + @"\Imagens\" + p.name;

                if (!File.Exists(path + ".png"))
                {

                    using (WebClient webclient = new WebClient())
                    {

                        webclient.DownloadFile(p.flag, path + ".svg");
                        webclient.Dispose();

                    }

                    try
                    {
                        using (MagickImage image = new MagickImage(path + ".svg"))
                        {

                            image.Write(path + ".png");
                            image.Dispose();
                        }
                    }
                    catch (Exception e)
                    {
                        DialogService.ShowMessage(e.Message, "Erro", MessageBoxButtons.OK);
                    }
                   

                    File.Delete(path + ".svg");


                }
             
            }

            toolStripProgressBar1.Value = 100;

            if (Directory.Exists("Data"))
            {

                DataService.DeleteData();

            }

            DataService.SaveData(ListaPaises);

            panel1.Enabled = true;

            CarregarCombo();

        }

        /// <summary>
        /// Carrega os paises da base de dados.
        /// </summary>
        private void LoadOffline()
        {

            ListaPaises = DataService.GetData();

            var x = File.GetLastWriteTime("Data/PaisesBD.sqlite");

            toolStripStatusLabel1.Text = "Carregada da base de dados. Última atualização foi em " + x;

            CarregarCombo();

        }

        /// <summary>
        /// Carrega os países para a combobox.
        /// </summary>
        private void CarregarCombo()
        {

            comboBox_regionselect.Items.Add("All");
            comboBox_regionselect.Items.Add("Africa");
            comboBox_regionselect.Items.Add("Americas");
            comboBox_regionselect.Items.Add("Asia");
            comboBox_regionselect.Items.Add("Europe");
            comboBox_regionselect.Items.Add("Oceania");

        }

        #endregion

    }
}
