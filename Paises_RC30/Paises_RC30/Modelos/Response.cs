﻿namespace Paises_RC30.Modelos
{

    public class Response
    {

        /// <summary>
        /// Verifica se existe  conexão ou não.
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Mostra uma mensagem.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// O que sai da classe.
        /// </summary>
        public object result { get; set; }

    }
}
