﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paises_RC30.Modelos
{
    public class RegionalBloc
    {

        /// <summary>
        /// As iniciais do grupo(Ex: EU, BRIC, AU. etc.).
        /// </summary>
        public string acronym { get; set; }

        /// <summary>
        /// Nome completo do grupo.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Outros acrónimos, caso existam, desse grupo.
        /// </summary>
        public List<object> otherAcronyms { get; set; }//Não vai ser incluído.

        /// <summary>
        /// Outros nomes completos, caso existam, desse grupo.
        /// </summary>
        public List<object> otherNames { get; set; }//Não vai ser incluído.

    }
}
