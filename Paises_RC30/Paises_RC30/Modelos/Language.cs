﻿namespace Paises_RC30.Modelos
{

    public class Language
    {

        /// <summary>
        /// Código do país só de duas letras.
        /// </summary>
        public string iso639_1 { get; set; }

        /// <summary>
        /// Código do país só de três letras.
        /// </summary>
        public string iso639_2 { get; set; }//Não vai ser incluído.

        /// <summary>
        /// Nome das linguagens existentes nesse país.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Nome das linguas nesse pais.
        /// </summary>
        public string nativeName { get; set; }//Não vai ser incluído.

    }
}
