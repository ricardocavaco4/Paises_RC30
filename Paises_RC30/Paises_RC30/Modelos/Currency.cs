﻿namespace Paises_RC30.Modelos
{

    public class Currency
    {

        /// <summary>
        /// Sigla da moeda.
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// Nome da moeda.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Símbolo da moeda, ex: £,$,€.
        /// </summary>
        public string symbol { get; set; }
    }
}
