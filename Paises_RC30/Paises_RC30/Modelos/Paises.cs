﻿namespace Paises_RC30.Modelos
{

    using System.Collections.Generic;

    public class Paises
    {

        /// <summary>
        /// Nome do País.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Dominio(s) da internet.
        /// </summary>
        public List<string> topLevelDomain { get; set; }

        /// <summary>
        /// Capital do país.
        /// </summary>
        public string capital { get; set; }

        /// <summary>
        /// Continente onde o país está localizado.
        /// </summary>
        public string region { get; set; }

        /// <summary>
        /// Em que região do continente se situa.
        /// </summary>
        public string subregion { get; set; }

        /// <summary>
        /// A quantidade de pessoas existentes.
        /// </summary>
        public int population { get; set; }

        /// <summary>
        /// Diferenças de tempo existentes no país.
        /// </summary>
        public List<string> timezones { get; set; }

        /// <summary>
        /// Países que rodeiam o país.
        /// </summary>
        public List<string> borders { get; set; }

        /// <summary>
        /// Nome do país da própria lingua.
        /// </summary>
        public string nativeName { get; set; }

        /// <summary>
        /// Lista de moedas existentes neste país.
        /// </summary>
        public List<Currency> currencies { get; set; }

        /// <summary>
        /// Lista de linguas faladas neste país.
        /// </summary>
        public List<Language> languages { get; set; }

        /// <summary>
        /// Link da imagem da bandeira em SVG.
        /// </summary>
        public string flag { get; set; }

        /// <summary>
        /// Códigos de dois caractéres que se usam que pertencem a um país(ex: PT, ES, IS, etc).
        /// </summary>
        public string alpha2Code { get; set; }

        /// <summary>
        /// Códigos de três caractéres que se usam que pertencem a um país(ex: PRT, ESP, ISL, etc).
        /// </summary>
        public string alpha3Code { get; set; }//Não vai ser incluído.

        /// <summary>
        /// Prefixos de números de telefone/telemovél.
        /// </summary>
        public List<string> callingCodes { get; set; }

        /// <summary>
        /// Outras maneiras de escrever o país.
        /// </summary>
        public List<string> altSpellings { get; set; }//Não vai ser incluído.

        /// <summary>
        /// Latitude e Longitude.
        /// </summary>
        public List<string> latlng { get; set; }

        /// <summary>
        /// Nome dos habitantes desse país.
        /// </summary>
        public string demonym { get; set; }

        /// <summary>
        /// Area total ocupada pelo país.
        /// </summary>
        public string area { get; set; }

        /// <summary>
        /// Coeficiente que demonstra a distribuição de rendimentos dos habitantes.
        /// </summary>
        public string gini { get; set; }

        /// <summary>
        /// Códigos de três digitos que se usam que pertencem a um país(ex: 620, 724, 352, etc).
        /// </summary>
        public string numericCode { get; set; }

        /// <summary>
        /// Traduções do nome do País em diversas linguagens.
        /// </summary>
        public Translations translations { get; set; }//Não vai ser incluído.

        /// <summary>
        /// A que grupo de paises este pais pertence.
        /// </summary>
        public List<RegionalBloc> regionalBlocs { get; set; }

        /// <summary>
        /// ???Letras de identificação do país, pode não existir???
        /// </summary>
        public string cioc { get; set; }//Não vai ser incluído.

        //Propriedades para receber da BD:

        /// <summary>
        /// Recebe a string das currencies da BD.
        /// </summary>
        public string Curr { get; set; }

        /// <summary>
        /// Recebe a string das Languages da BD.
        /// </summary>
        public string Lang { get; set; }

        /// <summary>
        /// Recebe a string dos nomes do regionalBlocs da BD.
        /// </summary>
        public string Regio_name { get; set; }

        /// <summary>
        /// Recebe a string dos acrónimos do regionalBlocs da BD.
        /// </summary>
        public string Regio_acron { get; set; }

        /// <summary>
        /// Recebe a latitude da BD.
        /// </summary>
        public string Lati { get; set; }

        /// <summary>
        /// Recebe a longitude da BD.
        /// </summary>
        public string Long { get; set; }
    }
}
