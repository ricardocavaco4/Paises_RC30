﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paises_RC30.Modelos
{
    public class Translations
    {

        /// <summary>
        /// Tradução em Alemão.
        /// </summary>
        public string de { get; set; }

        /// <summary>
        /// Tradução em Espanhol.
        /// </summary>
        public string es { get; set; }

        /// <summary>
        /// Tradução em Francês.
        /// </summary>
        public string fr { get; set; }

        /// <summary>
        /// Tradução em Japonês.
        /// </summary>
        public string ja { get; set; }

        /// <summary>
        /// Tradução em Italiano.
        /// </summary>
        public string it { get; set; }

        /// <summary>
        /// Tradução em Bretâo.
        /// </summary>
        public string br { get; set; }

        /// <summary>
        /// Tradução em Português.
        /// </summary>
        public string pt { get; set; }

        /// <summary>
        /// Tradução em Holandês.
        /// </summary>
        public string nl { get; set; }

        /// <summary>
        /// Tradução em Croácio.
        /// </summary>
        public string hr { get; set; }

        /// <summary>
        /// Tradução em Pérsio.
        /// </summary>
        public string fa { get; set; }

    }
}
